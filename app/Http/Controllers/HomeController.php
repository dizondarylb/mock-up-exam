<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;
/* controller sitemap
    - login page
    - register page
    - product page
*/
class HomeController extends Controller
{
    public function home()
    {
        return view('pages.login');
    }

    public function register()
    {
        return view('pages.register');
    }

    public function dashboard()
    {
        $products = Product::all();

        return view('pages.dashboard')
                ->with('products', $products);
    }
}
