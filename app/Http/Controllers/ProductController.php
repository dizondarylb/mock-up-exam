<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Product;
/* controller sitemap
    - add
    - update
    - delete
*/
class ProductController extends Controller
{
    public function add(Request $request)
    {

        if($request->input('id'))
        {
            $product = Product::where('id', $request->input('id'))->first();

            $product->name = $request->input('product');
            $product->price = $request->input('price');
            $product->save();

            return back()->with("message", "Product has been updated!");
        } 
        else 
        {
            $data = [
                'name' => $request->input('product'),
                'price' => $request->input('price')
            ];

            $this->create($data);

            return back()->with("message", "Product has been added!");
        }
    }

    public function delete(Request $request)
    {
        Product::where('id', $request->input('id'))->delete();

        return back()->with("message", "Product has been deleted!");
    }

    protected function create(array $data)
    {
        return Product::create([
            'name' => $data['name'],
            'price' => $data['price']
        ]);
    }
}
