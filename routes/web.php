<?php

use Illuminate\Support\Facades\Route;

/*
| Authenticated group
*/
Route::group(['middleware' => 'auth'], function () {

	/*
	| CSRF protection group
	*/
	Route::group(array('before' => 'csrf'), function() {
        Route::post('/product/add', 'ProductController@add')->name('add');
        Route::post('/product/delete', 'ProductController@delete')->name('delete');
    });
    
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
});

/*
| Unauthenticated group
*/
Route::group(['middleware' => 'guest'], function () {

    Route::get('/', 'HomeController@home')->name('home');
    Route::get('/login', 'HomeController@home')->name('login');
    Route::get('/register', 'HomeController@register')->name('register');
});