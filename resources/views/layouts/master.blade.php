<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mockup Exam - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/semantic.min.css') }}" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}" />

    <script type="text/javascript">
    var url = location.href;var public_url = url.indexOf("public");url = url.replace("public/", "");if(public_url > -1) { location.href= url; }
    </script>
</head>
<body>
    @yield('header')
    @yield('content')
    @yield('footer')

    <script type="text/javascript" src="{{ asset('public/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/app.js') }}"></script>
</body>
</html>