<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Mock Up Exam') }}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/semantic.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/css/app.css') }}">

        @livewireStyles

        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js" defer></script>
        <script type="text/javascript">
        var url = location.href;var public_url = url.indexOf("public");url = url.replace("public/", "");if(public_url > -1) { location.href= url; }
        </script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-dropdown')

            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <main>
                {{ $slot }}
            </main>
        </div>

        @stack('modals')

        <!-- @livewireScripts -->
        <script type="text/javascript" src="{{ asset('public/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ asset('public/js/app.js') }}"></script>
    </body>
</html>
