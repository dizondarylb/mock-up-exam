@extends('layouts.master')

@section('title', 'Home')

@section('header')
@endsection

@section('content')    
<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        </x-slot>

        <p class="ui blue ribbon label">Login</p>
        <br /><br />
        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('login') }}" class="ui form">
            @csrf

            <div class="field">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                    <i class="user icon"></i>
                </div>
            </div>

            <div class="field">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
                    <i class="lock icon"></i>
                </div>
            </div>

            <div class="field">
                <div for="remember_me" class="ui checkbox">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                    <label>{{ __('Remember me') }}</span>
                </div>
            </div>

            <div class="field">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
                <br /><br />

                <x-jet-button class="ui blue submit button">
                    {{ __('Login') }}
                </x-jet-button>

                <a href="{{ route('register') }}" class="ui green button">
                    <i class="signup icon"></i>
                    Register
                </a>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
@endsection

@section('footer')
@endsection