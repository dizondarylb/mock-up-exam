@extends('layouts.master')

@section('title', 'Home')

@section('header')
@endsection

@section('content')    
<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
        </x-slot>

        <p class="ui green ribbon label">Create an Account</p>
        <br /><br />
        <x-jet-validation-errors class="mb-4" />
        <form method="POST" action="{{ route('register') }}" class="ui form">
            @csrf

            <div class="field">
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                    <i class="user icon"></i>
                </div>
            </div>

            <div class="field">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                    <i class="mail icon"></i>
                </div>
            </div>

            <div class="field">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
                    <i class="lock icon"></i>
                </div>
            </div>

            <div class="field">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <div class="ui left icon input">
                    <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                    <i class="lock icon"></i>
                </div>
            </div>

            <div class="field">
                <a href="{{ route('home') }}">
                    {{ __('Already registered?') }}
                </a><br /><br />

                <x-jet-button class="ui green button">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>

@endsection

@section('footer')
@endsection