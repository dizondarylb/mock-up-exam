<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Mock Up Exam') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <div class="p-6 sm:px-20 bg-white border-b border-gray-200 cc_cursor">
                        <div class="mt-8 text-2xl cc_cursor">
                            Welcome! I'm Daryl Dizon.
                        </div>

                        <div class="mt-6 text-gray-500 cc_cursor">
                            <div class="ui two column grid">
                                <div class="column">
                                    <form method="POST" action="{{ route('add') }}"  class="ui form form-product">
                                        @csrf
                                        <input type="hidden" name="id">
                                        <div class="field">
                                            <label>Product Name</label>
                                            <input type="text" name="product" required>
                                        </div>
                                        <div class="field">
                                            <label>Price</label>
                                            <input type="number" name="price" min="1" required>
                                        </div>
                                        <input type="submit" class="ui green button" value="Add Product">
                                        <input type="button" class="ui button cancel" value="Cancel" style="display:none">
                                    </form>
                                </div>
                                <div class="column">
                                    <table class="ui celled table">
                                        <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($products))
                                            @foreach($products as $product)
                                            <tr>
                                                <td class="product" data-label="Product Name">{{ $product->name }}</td>
                                                <td class="price" data-label="Price">{{ $product->price }}</td>
                                                <td data-label="Action"><button class="ui primary mini button edit" data-id="{{ $product->id }}">Edit</button><button class="ui red mini button delete" data-id="{{ $product->id }}">Delete</button></td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
