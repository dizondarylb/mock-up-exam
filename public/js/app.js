
$(function(){

    $('.edit').on("click", function(){

        var id = $(this).attr("data-id");
        var product = $(this).parent().parent().children('td.product').text();
        var price = $(this).parent().parent().children('td.price').text();
        
        $(".form-product input[name=id]").val(id);
        $(".form-product input[name=product]").val(product);
        $(".form-product input[name=price]").val(price);

        $(".form-product input[type=submit]").val("Update Product");
        $(".form-product .button.cancel").show();
    });
    $(".form-product .button.cancel").on("click", function(){
        $(".form-product input[name=id]").val("");
        $(".form-product input[name=product]").val("");
        $(".form-product input[name=price]").val("");

        $(".form-product input[type=submit]").val("Add Product");
        $(".form-product .button.cancel").hide();
    });

    $('.delete').on("click", function(){
        var element = $(this);
        var id = $(element).attr("data-id");
        $(element).parent().parent().fadeOut(1000);
        setTimeout(function(){ $(element).parent().parent().remove(); }, 3000);
        ajaxDeleteProduct(id);
    });
});

function ajaxDeleteProduct(id) {
    return $.ajax({
        url: "/mockup-exam/public/product/delete",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        data: {'id': id}
    });
}